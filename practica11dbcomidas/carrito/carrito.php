<?php 
  session_start();
    if(isset($_POST['submit'])){ 
          
        foreach($_POST['quantity'] as $key => $val) { 
            if($val==0) { 
                unset($_SESSION['carrito'][$key]); 
            }else{ 
                $_SESSION['carrito'][$key]['quantity']=$val; 
            } 
        } 
          
    } 
  
?> 
  
<h1>View cart</h1> 
<a href="index.php?page=productos">Go back to the products page.</a> 
<form method="post" action="index.php?page=carrito"> 
      
    <table> 
          
        <tr> 
            <th>Name</th> 
            <th>Quantity</th> 
            <th>Price</th> 
            <th>Items Price</th> 
        </tr> 
          
        <?php 
          require("includes/conexion.php");
            $sql="SELECT * FROM comida WHERE CodigoComida IN ("; 
                      
                    foreach($_SESSION['carrito'] as $id => $value) { 
                        $sql.=$id.","; 
                    } 
                      
                    $sql=substr($sql, 0, -1).") ORDER BY NombreComida ASC"; 
                    $query=mysqli_query($conexion, $sql); 
                    $totalprice=0; 
                    while($row=mysqli_fetch_array($query)){ 
                        $subtotal=$_SESSION['carrito'][$row['CodigoComida']]['quantity']*$row['PrecioComida']; 
                        $totalprice+=$subtotal; 
                    ?> 
                        <tr> 
                            <td><?php echo $row['NombreComida'] ?></td> 
                            <td><input type="text" name="quantity[<?php echo $row['NodigoComida'] ?>]" size="5" value="<?php echo $_SESSION['carrito'][$row['CodigoComida']]['quantity'] ?>" /></td> 
                            <td><?php echo $row['PrecioComida'] ?>$</td> 
                            <td><?php echo $_SESSION['carrito'][$row['PodigoComida']]['quantity']*$row['PrecioComida'] ?>$</td> 
                        </tr> 
                    <?php 
                          
                    } 
        ?> 
                    <tr> 
                        <td colspan="4">Total Price: <?php echo $totalprice ?></td> 
                    </tr> 
          
    </table> 
    <br /> 
    <button type="submit" name="submit">Update Cart</button> 
</form> 
<br /> 
<p>To remove an item set its quantity to 0. </p>