-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-02-2018 a las 13:09:13
-- Versión del servidor: 10.1.26-MariaDB
-- Versión de PHP: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bdcomidas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `numeroCliente` int(11) NOT NULL,
  `nombreCliente` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `apellidoCliente` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `sexoCliente` enum('Mujer','Hombre') COLLATE utf8_spanish_ci NOT NULL,
  `direccionCliente` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `telefonoCliente` varchar(9) COLLATE utf8_spanish_ci NOT NULL,
  `codigoTipoCliente` int(11) NOT NULL,
  `usuarioCliente` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `claveCliente` varchar(30) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`numeroCliente`, `nombreCliente`, `apellidoCliente`, `sexoCliente`, `direccionCliente`, `telefonoCliente`, `codigoTipoCliente`, `usuarioCliente`, `claveCliente`) VALUES
(14, 'Juan', 'García', 'Hombre', 'Cl. Sol, 3 Valencia', '653303035', 1, 'jgarcia', '123'),
(15, 'Ana', 'Puig', 'Mujer', 'Cl. Marte, 5 Valencia', '630505050', 2, 'apuig', '123'),
(16, 'Jesús', 'Vila', 'Hombre', 'Falconera 29', '650861510', 2, 'jevi', 'reinadelart');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comidas`
--

CREATE TABLE `comidas` (
  `codigoComida` int(11) NOT NULL,
  `nombreComida` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `descripcionComida` varchar(60) COLLATE utf8_spanish_ci NOT NULL,
  `precioComida` decimal(4,2) NOT NULL,
  `codigoTipoComida` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `comidas`
--

INSERT INTO `comidas` (`codigoComida`, `nombreComida`, `descripcionComida`, `precioComida`, `codigoTipoComida`) VALUES
(1, 'Paella valenciana', 'Arroz con pollo y conejo', '6.50', 1),
(2, 'Chuletas de cordero', 'Cordero lechal', '7.50', 3),
(5, 'Macarrones', 'Pasta con salsa bolognesa grat', '2.00', 2),
(6, 'Dorada', 'Dorada a la sal al horno', '12.00', 4),
(7, 'Chuletón ternera', 'Chuletón de ternera a la brasa con guarnición.', '20.50', 3),
(18, 'Arroz con Bogavante', 'Arroz meloso con Bogavante', '12.50', 1),
(19, 'Arroz del Senyoret', 'Arroz de marisco con las gambas peladas', '15.25', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalleventas`
--

CREATE TABLE `detalleventas` (
  `cantidadVenta` int(11) NOT NULL,
  `ImporteVenta` decimal(6,2) NOT NULL,
  `numeroVenta` int(11) NOT NULL,
  `codigoComida` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleados`
--

CREATE TABLE `empleados` (
  `codigoEmpleado` int(11) NOT NULL,
  `nombreEmpleado` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `apellidoEmpleado` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `sexoEmpleado` enum('Mujer','Hombre') COLLATE utf8_spanish_ci NOT NULL,
  `direccionEmpleado` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `telefonoEmpleado` varchar(9) COLLATE utf8_spanish_ci NOT NULL,
  `usuarioEmpleado` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `claveEmpleado` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `codigoTipoEmpleado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `empleados`
--

INSERT INTO `empleados` (`codigoEmpleado`, `nombreEmpleado`, `apellidoEmpleado`, `sexoEmpleado`, `direccionEmpleado`, `telefonoEmpleado`, `usuarioEmpleado`, `claveEmpleado`, `codigoTipoEmpleado`) VALUES
(1, 'José', 'Pérez', 'Hombre', 'Cl. Sol, 5 Valencia', '650505050', 'jperez', '123', 1),
(2, 'Jesús', 'Vila', 'Hombre', 'C/Falconera-29', '650861510', 'jevi', '1234', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoclientes`
--

CREATE TABLE `tipoclientes` (
  `codigoTipoCliente` int(11) NOT NULL,
  `nombreTipoCliente` varchar(30) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tipoclientes`
--

INSERT INTO `tipoclientes` (`codigoTipoCliente`, `nombreTipoCliente`) VALUES
(1, 'Básico'),
(2, 'VIP');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipocomidas`
--

CREATE TABLE `tipocomidas` (
  `codigoTipoComida` int(11) NOT NULL,
  `nombreTipoComida` varchar(30) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tipocomidas`
--

INSERT INTO `tipocomidas` (`codigoTipoComida`, `nombreTipoComida`) VALUES
(1, 'Arroces'),
(2, 'Pasta'),
(3, 'Carne'),
(4, 'Pescados'),
(5, 'Postres'),
(6, 'Bebidas'),
(7, 'Picaeta'),
(8, 'Ensaladas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoempleados`
--

CREATE TABLE `tipoempleados` (
  `codigoTipoEmpleado` int(11) NOT NULL,
  `nombreTipoEmpleado` varchar(30) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tipoempleados`
--

INSERT INTO `tipoempleados` (`codigoTipoEmpleado`, `nombreTipoEmpleado`) VALUES
(1, 'Encargado'),
(2, 'Camarero'),
(3, 'Cocinero');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE `ventas` (
  `numeroVenta` int(11) NOT NULL,
  `fechaVenta` date NOT NULL,
  `totalVenta` decimal(6,2) NOT NULL,
  `codigoEmpleado` int(11) NOT NULL,
  `numeroCliente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`numeroCliente`),
  ADD KEY `codigoTipoCliente` (`codigoTipoCliente`);

--
-- Indices de la tabla `comidas`
--
ALTER TABLE `comidas`
  ADD PRIMARY KEY (`codigoComida`),
  ADD KEY `codigoTipoComida` (`codigoTipoComida`);

--
-- Indices de la tabla `detalleventas`
--
ALTER TABLE `detalleventas`
  ADD KEY `numeroVenta` (`numeroVenta`,`codigoComida`),
  ADD KEY `codigoComida` (`codigoComida`);

--
-- Indices de la tabla `empleados`
--
ALTER TABLE `empleados`
  ADD PRIMARY KEY (`codigoEmpleado`),
  ADD KEY `codigoTipoEmpleado` (`codigoTipoEmpleado`);

--
-- Indices de la tabla `tipoclientes`
--
ALTER TABLE `tipoclientes`
  ADD PRIMARY KEY (`codigoTipoCliente`);

--
-- Indices de la tabla `tipocomidas`
--
ALTER TABLE `tipocomidas`
  ADD PRIMARY KEY (`codigoTipoComida`);

--
-- Indices de la tabla `tipoempleados`
--
ALTER TABLE `tipoempleados`
  ADD PRIMARY KEY (`codigoTipoEmpleado`);

--
-- Indices de la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`numeroVenta`),
  ADD KEY `codigoEmpleado` (`codigoEmpleado`),
  ADD KEY `numeroCliente` (`numeroCliente`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `numeroCliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `comidas`
--
ALTER TABLE `comidas`
  MODIFY `codigoComida` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `empleados`
--
ALTER TABLE `empleados`
  MODIFY `codigoEmpleado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tipoclientes`
--
ALTER TABLE `tipoclientes`
  MODIFY `codigoTipoCliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tipocomidas`
--
ALTER TABLE `tipocomidas`
  MODIFY `codigoTipoComida` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `tipoempleados`
--
ALTER TABLE `tipoempleados`
  MODIFY `codigoTipoEmpleado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `ventas`
--
ALTER TABLE `ventas`
  MODIFY `numeroVenta` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD CONSTRAINT `clientes_ibfk_1` FOREIGN KEY (`codigoTipoCliente`) REFERENCES `tipoclientes` (`codigoTipoCliente`);

--
-- Filtros para la tabla `comidas`
--
ALTER TABLE `comidas`
  ADD CONSTRAINT `comidas_ibfk_1` FOREIGN KEY (`codigoTipoComida`) REFERENCES `tipocomidas` (`codigoTipoComida`);

--
-- Filtros para la tabla `detalleventas`
--
ALTER TABLE `detalleventas`
  ADD CONSTRAINT `detalleventas_ibfk_1` FOREIGN KEY (`numeroVenta`) REFERENCES `ventas` (`numeroVenta`),
  ADD CONSTRAINT `detalleventas_ibfk_2` FOREIGN KEY (`codigoComida`) REFERENCES `comidas` (`codigoComida`);

--
-- Filtros para la tabla `empleados`
--
ALTER TABLE `empleados`
  ADD CONSTRAINT `empleados_ibfk_1` FOREIGN KEY (`codigoTipoEmpleado`) REFERENCES `tipoempleados` (`codigoTipoEmpleado`);

--
-- Filtros para la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD CONSTRAINT `ventas_ibfk_1` FOREIGN KEY (`numeroCliente`) REFERENCES `clientes` (`numeroCliente`),
  ADD CONSTRAINT `ventas_ibfk_2` FOREIGN KEY (`codigoEmpleado`) REFERENCES `empleados` (`codigoEmpleado`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
