<?php 
require("includes/conexion.php");
  
    if(isset($_GET['action']) && $_GET['action']=="add"){ 
          
        $id=intval($_GET['id']); 
          
        if(isset($_SESSION['carrito'][$id])){ 
              
            $_SESSION['carrito'][$id]['quantity']++; 
              
        }else{ 
              
            $sql_s="SELECT * FROM comidas 
                WHERE CodigoComida={$id}"; 
            $query_s=mysqli_query($conexion, $sql_s); 
            if(mysqli_num_rows($query_s)!=0){ 
                $row_s=mysqli_fetch_array($query_s); 
                  
                $_SESSION['carrito'][$row_s['CodigoComida']]=array( 
                        "quantity" => 1, 
                        "price" => $row_s['PrecioComida'] 
                    ); 
                  
                  
            }else{ 
                  
                $message="El código de esta comida no es válido!"; 
                  
            } 
              
        } 
          
    } 
  
?> 
    <h1>Lista de Comidas</h1> 
    <?php 
        if(isset($message)){ 
            echo "<h2>$message</h2>"; 
        } 
    ?> 
    <table> 
        <tr> 
            <th>Name</th> 
            <th>Description</th> 
            <th>Price</th> 
            <th>Action</th> 
        </tr> 
          
        <?php 
          
            $sql="SELECT * FROM comida ORDER BY NombreComida ASC"; 
            $query=mysqli_query($conexion, $sql); 
              
            while ($row=mysqli_fetch_array($query)) { 
                  
        ?> 
            <tr> 
                <td><?php echo $row['NombreComida'] ?></td> 
                <td><?php echo $row['DescripcionComida'] ?></td> 
                <td><?php echo $row['PrecioComida'] ?>$</td> 
                <td><a href="index.php?page=productos&action=add&id=<?php echo $row['CodigoComida'] ?>">Añadir al carrito</a></td> 
            </tr> 
        <?php 
                  
            } 
          
        ?> 
          
    </table>